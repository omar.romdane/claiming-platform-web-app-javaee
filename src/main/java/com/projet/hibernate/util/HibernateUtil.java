package com.projet.hibernate.util;

import java.sql.DriverManager;
import java.util.Properties;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;
import com.projet.entity.Administrator;
import com.projet.entity.Claim;
import com.projet.entity.ClaimType;
import com.projet.entity.Student;
import com.projet.entity.User;

public class HibernateUtil {
	private static SessionFactory sessionFactory = null;

	public static SessionFactory getSessionFactory() {
		if (sessionFactory == null) {
			try {
				Configuration configuration = new Configuration();
				Properties settings = new Properties();
				settings.put(Environment.DRIVER, "com.mysql.cj.jdbc.Driver");
				settings.put(Environment.URL, "jdbc:mysql://localhost:3300/claim_management");	
				settings.put(Environment.USER, "root");
				settings.put(Environment.PASS, "root");
				settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5Dialect");
				settings.put(Environment.SHOW_SQL, "true");
				settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
				settings.put(Environment.HBM2DDL_AUTO, "create-drop");
				configuration.setProperties(settings);
				configuration.addAnnotatedClass(User.class);
				configuration.addAnnotatedClass(Student.class);
				configuration.addAnnotatedClass(Administrator.class);
				configuration.addAnnotatedClass(Claim.class);
				configuration.addAnnotatedClass(ClaimType.class);
	
				// Test the database connectivity by trying to establish a connection
				DriverManager.getConnection(settings.getProperty(Environment.URL), 
					settings.getProperty(Environment.USER), settings.getProperty(Environment.PASS));
				System.out.println("Database connection successful");
				ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
						.applySettings(configuration.getProperties()).build();
				sessionFactory = configuration.buildSessionFactory(serviceRegistry);
				return sessionFactory;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return sessionFactory;
	}
	
};

