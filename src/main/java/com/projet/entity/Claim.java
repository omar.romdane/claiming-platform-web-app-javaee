package com.projet.entity;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="claims")
public class Claim {
	
	public Claim(String title, String description, ClaimType claimType, Student student) {
		super();
		this.title = title;
		this.description = description;
		this.claimType = claimType;
		this.student = student;
	}

	@Id
	@Column(name = "claim_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name="title")
	private String title;
	
	@Column(name="description")
	private String description;
	
	@ManyToOne(targetEntity=ClaimType.class)
	private ClaimType claimType;
	
	@ManyToOne(targetEntity=Student.class)
	@JoinColumn(name = "student_id")
	private Student student;
	
	public ClaimType getClaimType() {
		return claimType;
	}

	public void setClaimType(ClaimType claimType) {
		this.claimType = claimType;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}
	
	public Claim() {
		super();
	}
	
	public Claim(String title, String description) {
		super();
		this.title = title;
		this.description = description;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public boolean equals(Object o) {
	    if (this == o) return true;
	    if (o == null || getClass() != o.getClass()) return false;
	    Claim claim = (Claim) o;
	    return Objects.equals(id, claim.id);
	}

	@Override
	public int hashCode() {
	    return Objects.hash(id);
	}
	
}

