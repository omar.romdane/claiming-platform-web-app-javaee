package com.projet.entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;


@Entity(name="Student")
public class Student extends User{
	
	@Column(name="first_name")
	private String firstName;
	
	@Column(name="last_name")
	private String lastName;
	
	@OneToMany(targetEntity=Claim.class ,mappedBy = "student",cascade = CascadeType.ALL, orphanRemoval = true,fetch = FetchType.EAGER)
	private List<Claim> claims = new ArrayList<Claim>();

	public Student() {
		super();
	}
	
	public void addClaim(Claim claim) {
	    if (!this.claims.contains(claim)) {
	    	claim.setStudent(this);
	    	this.claims.add(claim);
	    }
	 }
	
	public List<Claim> getClaims() {
		return claims;
	}

	public void setClaims(List<Claim> claims) {
		this.claims = claims;
	}

	public Student(String email, String password, boolean active, String firstName, String lastName) {
		super(email, password, active);
		this.firstName = firstName;
		this.lastName = lastName;	
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
}

