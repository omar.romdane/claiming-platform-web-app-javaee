package com.projet.entity;
import javax.persistence.Entity;
@Entity(name="administrator")
public class Administrator extends User{
	
	public Administrator() {
		super();
	}

	public Administrator(String email, String password, boolean active) {
		super(email, password, active);
	}
}
