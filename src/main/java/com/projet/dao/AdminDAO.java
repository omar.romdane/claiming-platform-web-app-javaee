package com.projet.dao;

import com.projet.entity.Administrator;

public interface AdminDAO {
	public Administrator findByEmailAndPassword(String email, String password);
}
