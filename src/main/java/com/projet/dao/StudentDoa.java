package com.projet.dao;

import java.util.List;
import com.projet.entity.Student;



public interface StudentDoa {
	public Student findById(int id);
	public List<Student> findAll();
	public void save(Student student);
	public void update(Student student);
	public void delete(Student student);
	public Student findByEmailAndPassword(String email, String password);
	public Student findByEmail(String email);
}
