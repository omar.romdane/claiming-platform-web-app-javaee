package com.projet.dao;

import java.util.List;

import com.projet.entity.Claim;
import com.projet.entity.ClaimType;
import com.projet.entity.Student;

public interface ClaimDOA {
	void addClaim(Claim claim);
	void addClaimType(ClaimType claimType);
	List<Claim> listClaims();
	List<ClaimType> listClaimTypes();
	boolean existClaimType(String type);
	ClaimType getClaimsByType(String type);
	List<Claim> listClaimsByStudent(Student student);
	ClaimType findClaimTypeById(int id);
	Claim findClaimById(int id);
	public void deleteClaim(Claim claim);
	public void deleteClaimType(ClaimType claimType);
	public void updateClaimType(ClaimType claimType);
	List<Claim> findClaimsByClaimType(ClaimType claimType);
	public List<Claim> findClaimsByTitleOrDescription(String search);
}
