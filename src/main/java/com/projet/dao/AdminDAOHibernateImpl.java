package com.projet.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import com.projet.entity.Administrator;
import com.projet.hibernate.util.HibernateUtil;

public class AdminDAOHibernateImpl implements AdminDAO{
	private static SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

	@Override
	public Administrator findByEmailAndPassword(String email, String password) {
		Session session = sessionFactory.openSession();
	    Transaction transaction = null;
	    Administrator administrator = null;
	    try {
	        transaction = session.beginTransaction();
	        administrator = (Administrator) session.createQuery("FROM Administrator WHERE email = :email and password = :password")
	                .setParameter("email", email)
	                .setParameter("password", password)
	                .uniqueResult();
	        transaction.commit();
	    } catch (Exception e) {
	        if (transaction != null) {
	            transaction.rollback();
	        }
	        e.printStackTrace();
	    } finally {
	        session.close();
	    }
	    return administrator;
	}

}
