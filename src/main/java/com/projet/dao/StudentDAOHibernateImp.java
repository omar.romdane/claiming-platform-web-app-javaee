package com.projet.dao;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import com.projet.hibernate.util.HibernateUtil;
import com.projet.entity.Student;
public class StudentDAOHibernateImp implements StudentDoa{
	private static SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

	@Override
	public Student findById(int id){
		Session session = HibernateUtil.getSessionFactory().openSession();
	    Transaction transaction = null;
	    Student student = null;
	    try{
	    	transaction = session.beginTransaction();
	    	student = session.get(Student.class, id);
	    	transaction.commit();
	    }catch(Exception e){
	    	if(transaction != null){
	    		transaction.rollback();
	    	}
	    	e.printStackTrace();
	    }finally{
	    	session.close();
	    }
	    return student;
	}

	@Override
	public List<Student> findAll() {
	    Session session = HibernateUtil.getSessionFactory().openSession();
	    Transaction transaction = null;
	    List<Student> students = null;
	    try {
	      transaction = session.beginTransaction();
	      students = session.createQuery("from Student",Student.class).list();
	      transaction.commit();
	    } catch (Exception e) {
	      if (transaction != null) {
	        transaction.rollback();
	      }
	      e.printStackTrace();
	    } finally {
	      session.close();
	    }
	    return students;
	}

	@Override
	public void save(Student student) {
	    Session session = HibernateUtil.getSessionFactory().openSession();
	    Transaction transaction = null;
	    try{
	    	transaction = session.beginTransaction();
	        session.persist(student);
	        transaction.commit();
	    }catch (Exception e) {
	    	if (transaction != null){
	    		transaction.rollback();
	    	}
	    	e.printStackTrace();
	    }finally{
	    	session.close();
	    }
	}

	@Override
	public void update(Student student) {
	    Session session = HibernateUtil.getSessionFactory().openSession();
	    Transaction transaction = null;
	    try{
	    	transaction = session.beginTransaction();
	    	session.update(student);
	    	transaction.commit();
	    }catch (Exception e){
	    	if (transaction != null) {
	    		transaction.rollback();
	    	}
	    	e.printStackTrace();
	    }finally{
	    	session.close();
	    }
	}

	@Override
	public void delete(Student student) {
	    Session session = HibernateUtil.getSessionFactory().openSession();
	    Transaction transaction = null;
	    try {
	      transaction = session.beginTransaction();
	      session.delete(student);
	      transaction.commit();
	    } catch (Exception e) {
	      if (transaction != null) {
	        transaction.rollback();
	      }
	      e.printStackTrace();
	    } finally {
	      session.close();
	    }
	}

	@Override
	public Student findByEmailAndPassword(String email, String password) {
	    Session session = HibernateUtil.getSessionFactory().openSession();
	    Transaction transaction = null;
	    Student student = null;
	    try {
	        transaction = session.beginTransaction();
	        student = (Student) session.createQuery("FROM Student WHERE email = :email and password = :password")
	                .setParameter("email", email)
	                .setParameter("password", password)
	                .uniqueResult();
	        transaction.commit();
	    } catch (Exception e) {
	        if (transaction != null) {
	            transaction.rollback();
	        }
	        e.printStackTrace();
	    } finally {
	        session.close();
	    }
	    return student;
	}

	@Override
	public Student findByEmail(String email) {
	    Session session = HibernateUtil.getSessionFactory().openSession();
	    Transaction transaction = null;
	    Student student = null;
	    try {
	        transaction = session.beginTransaction();
	        student = session.createQuery("FROM Student WHERE email = :email", Student.class)
	                .setParameter("email", email)
	                .uniqueResult();
	        transaction.commit();
	    } catch (Exception e) {
	        if (transaction != null) {
	            transaction.rollback();
	        }
	        e.printStackTrace();
	    } finally {
	        session.close();
	    }
	    return student;
	}
}
