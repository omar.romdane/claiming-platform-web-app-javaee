package com.projet.dao;

import java.util.List;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import com.projet.entity.Claim;
import com.projet.entity.ClaimType;
import com.projet.entity.Student;
import com.projet.hibernate.util.HibernateUtil;

public class ClaimDAOHibernateImp implements ClaimDOA{

	@Override
	public void addClaim(Claim claim) {
		Session session = HibernateUtil.getSessionFactory().openSession();
	    Transaction transaction = session.beginTransaction();
	    try{
	    	session.save(claim);
	    	session.flush();
	    	transaction.commit();
	    }catch(HibernateException e){
	    	transaction.rollback();
	    	throw e;
	    }finally{
	    	session.close();
	    }
		
	}

	@Override
	public void addClaimType(ClaimType ClaimType) {
	    Session session = HibernateUtil.getSessionFactory().openSession();
	    Transaction transaction = session.beginTransaction();
	    try{
	    	session.persist(ClaimType);
	    	session.flush();
	    	transaction.commit();
	    }catch(HibernateException e){
	    	transaction.rollback();
	    	throw e;
	    }finally{
	    	session.close();
	    }
	}

	@Override
	public List<Claim> listClaims() {
		Session session = HibernateUtil.getSessionFactory().openSession();
	    Transaction transaction = session.beginTransaction();
	    try{
	    	TypedQuery<Claim> query = session.createQuery("FROM Claim", Claim.class);
	    	List<Claim> claims = query.getResultList();
	    	session.flush();
	    	transaction.commit();
	    	return claims;
	    }catch (HibernateException e) {
	    	transaction.rollback();
	    	throw e;
	    }finally{
	    	session.close();
	    }
	}

	@Override
	public List<ClaimType> listClaimTypes() {
	    Session session = HibernateUtil.getSessionFactory().openSession();
	    Transaction transaction = session.beginTransaction();
	    try{
	    	TypedQuery<ClaimType> query = session.createQuery("FROM ClaimType", ClaimType.class);
	    	List<ClaimType> claimTypes = query.getResultList();
	    	session.flush();
	    	transaction.commit();
	    	return claimTypes;
	    }catch(HibernateException e){
	    	transaction.rollback();
	    	throw e;
	    }finally{
	    	session.close();
	    }
	}

	@Override
	public boolean existClaimType(String type) {
	    Session session = HibernateUtil.getSessionFactory().openSession();
	    Transaction transaction = session.beginTransaction();
	    try{
	    	Query<ClaimType> query = session.createQuery("FROM ClaimType WHERE type = :type", ClaimType.class);
	    	query.setParameter("type", type);
	    	ClaimType claimType = query.uniqueResult();
	    	boolean exists = (claimType != null);
	    	session.flush();
	    	transaction.commit();
	    	return exists;
	    }catch(HibernateException e){
	    	transaction.rollback();
	    	throw e;
	    }finally{
	    	session.close();
	    }
	}

	@Override
	public ClaimType getClaimsByType(String type) {
		Session session = HibernateUtil.getSessionFactory().openSession();
	    Transaction transaction = session.beginTransaction();
	    try{
	    	TypedQuery<ClaimType> query = session.createQuery("FROM ClaimType WHERE type = :type", ClaimType.class);
	    	query.setParameter("type", type);
	    	ClaimType claimType = ((Query<ClaimType>) query).uniqueResult();
	      	session.flush();
	      	transaction.commit();
	      	return claimType;
	    }catch(HibernateException e){
	    	transaction.rollback();
	    	throw e;
	    } finally {
	    	session.close();
	    }
	}


	@Override
	public List<Claim> listClaimsByStudent(Student student){
	    Session session = HibernateUtil.getSessionFactory().openSession();
	    Transaction transaction = session.beginTransaction();
	    try{
	    	TypedQuery<Claim> query = session.createQuery("FROM Claim WHERE student = :student", Claim.class);
	    	query.setParameter("student", student);
	    	List<Claim> claims = query.getResultList();
	      	session.flush();
	      	transaction.commit();
	      	return claims;
	    }catch(HibernateException e){	
	    	transaction.rollback();
	    	throw e;
	    } finally {
	    	session.close();
	    }
	}
	public List<Claim> getClaims(int studentId) {
		StudentDoa studentDao = new StudentDAOHibernateImp();
	    Student student = studentDao.findById(studentId);
	    if (student == null) {
	        // student with given id does not exist
	        throw new WebApplicationException(Response.Status.NOT_FOUND);
	    }
	    List<Claim> claims = listClaimsByStudent(student);
	    return claims;
	}
	@Override
	public ClaimType findClaimTypeById(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	    Transaction transaction = null;
	    ClaimType claimType = null;
	    try{
	    	transaction = session.beginTransaction();
	    	claimType = session.get(ClaimType.class, id);
	    	transaction.commit();
	    }catch(Exception e){
	    	if(transaction != null){
	    		transaction.rollback();
	    	}
	    	e.printStackTrace();
	    }finally{
	    	session.close();
	    }
	    return claimType;
	}

	@Override
	public Claim findClaimById(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	    Transaction transaction = null;
	    Claim claim = null;
	    try{
	    	transaction = session.beginTransaction();
	    	claim = session.get(Claim.class, id);
	    	transaction.commit();
	    }catch(Exception e){
	    	if(transaction != null){
	    		transaction.rollback();
	    	}
	    	e.printStackTrace();
	    }finally{
	    	session.close();
	    }
	    return claim;
	}
	public void deleteClaim(Claim claim) {
	    Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	    session.beginTransaction();
	    session.delete(claim);
	    session.getTransaction().commit();
	}

	@Override
	public void deleteClaimType(ClaimType claimType) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	    session.beginTransaction();
	    session.remove(claimType);
	    session.getTransaction().commit();
		
	}
	public void updateClaimType(ClaimType claimType) {
	    Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	    session.beginTransaction();
	    session.update(claimType);
	    session.getTransaction().commit();
	}

	@Override
	public List<Claim> findClaimsByClaimType(ClaimType claimType) {
		Session session = HibernateUtil.getSessionFactory().openSession();
	    Transaction transaction = session.beginTransaction();
	    try{
	    	TypedQuery<Claim> query = session.createQuery("FROM Claim WHERE claimType = :claimType", Claim.class);
	    	query.setParameter("claimType", claimType);
	    	List<Claim> claims = query.getResultList();
	      	session.flush();
	      	transaction.commit();
	      	return claims;
	    }catch(HibernateException e){	
	    	transaction.rollback();
	    	throw e;
	    } finally {
	    	session.close();
	    }
	}
	public List<Claim> findClaimsByTitleOrDescription(String search) {
	    Session session = HibernateUtil.getSessionFactory().openSession();
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    CriteriaQuery<Claim> query = builder.createQuery(Claim.class);
	    Root<Claim> root = query.from(Claim.class);

	    Predicate titlePredicate = builder.like(root.get("title"), "%" + search + "%");
	    Predicate descriptionPredicate = builder.like(root.get("description"), "%" + search + "%");
	    Predicate searchPredicate = builder.or(titlePredicate, descriptionPredicate);

	    query.where(searchPredicate);
	    List<Claim> claims = session.createQuery(query).getResultList();

	    return claims;
	}
}
