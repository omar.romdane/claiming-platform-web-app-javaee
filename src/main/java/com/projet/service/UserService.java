package com.projet.service;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import com.projet.dao.AdminDAO;
import com.projet.dao.AdminDAOHibernateImpl;
import com.projet.dao.StudentDAOHibernateImp;
import com.projet.dao.StudentDoa;
import com.projet.entity.Administrator;
import com.projet.entity.Student;

public class UserService {

    private StudentDoa studentDao = new StudentDAOHibernateImp();
    private AdminDAO administratorDao = new AdminDAOHibernateImpl();

    public void createStudent(Student student) {
        String hashedPassword = hashPassword(student.getPassword());
        student.setPassword(hashedPassword);
        studentDao.save(student);
    }

    private String hashPassword(String password) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(password.getBytes());
            byte[] bytes = md.digest();
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public Student authenticateStudent(String email, String password) {
    	password = hashPassword(password);
        Student student = studentDao.findByEmailAndPassword(email, password);
        return student;
    }
    
    
    public Boolean isValidPassword(String password) {
    	return true;
    }

	public Administrator authenticateAdministrator(String email, String password) {
		password = hashPassword(password);
        Administrator administrator = administratorDao.findByEmailAndPassword(email, password);
        return administrator;
	}
    
}

