package com.projet.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.projet.dao.ClaimDAOHibernateImp;
import com.projet.dao.ClaimDOA;
import com.projet.entity.Claim;
import com.projet.entity.ClaimType;


@WebServlet(name="adminClaims",urlPatterns = "/admin/claims/*")
public class AdminClaimController extends HttpServlet {

	private static final long serialVersionUID = 2329707072398366283L;
	private ClaimDOA claimDao = new ClaimDAOHibernateImp();
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if (action == null) {
            listClaims(request, response);
        } else if (action.equals("view")) {
            viewClaim(request, response);
        } else if (action.equals("delete")) {
            deleteClaim(request, response);
        } else {
            response.sendRedirect("claims");
        }
    }
    private void listClaims(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String claimTypeId = request.getParameter("claimTypeId");
        String search = request.getParameter("search");
        ClaimType selectedClaimType = null;
        List<Claim> claims;
        if (claimTypeId != null && !claimTypeId.isEmpty()) {
            int id = Integer.parseInt(claimTypeId);
            ClaimType claimType = claimDao.findClaimTypeById(id);
            selectedClaimType = claimType;
            List<Claim> claimsByType = claimDao.findClaimsByClaimType(claimType);
            if (search != null && !search.isEmpty()) {
                claims = claimDao.findClaimsByTitleOrDescription(search);
                claims.retainAll(claimsByType);
            } else {
                claims = claimsByType;
            }
        } else if (search != null && !search.isEmpty()) {
            claims = claimDao.findClaimsByTitleOrDescription(search);
        } else {
            claims = claimDao.listClaims();
        }
        List<ClaimType> claimTypes = claimDao.listClaimTypes();
        request.setAttribute("search", search);
        request.setAttribute("claims", claims);
        request.setAttribute("claimTypes", claimTypes);
        request.setAttribute("selectedClaimType", selectedClaimType);
        request.getRequestDispatcher("/WEB-INF/AdminClaims.jsp").forward(request, response);
    }


    private void viewClaim(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        Claim claim = claimDao.findClaimById(id);
        request.setAttribute("claim", claim);
        request.getRequestDispatcher("/WEB-INF/AdminViewClaim.jsp").forward(request, response);
    }

    private void deleteClaim(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        Claim claim = claimDao.findClaimById(id);
        claimDao.deleteClaim(claim);
        response.sendRedirect("claims");
    }
}
