package com.projet.controller;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.projet.entity.Student;
import com.projet.service.UserService;

@WebServlet(name="login",urlPatterns = "/login/*")
public class LoginController extends HttpServlet{
	private UserService userService = new UserService();
	private static final long serialVersionUID = 402574224090079356L;
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/Login.jsp").include(request, response);   
	}
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        if(email.equals(null) || password.equals(null) || email.isEmpty() || password.isEmpty()){
        	request.setAttribute("message", "Please fill out all fields.");
        	doGet(request, response);
        	return;
        }
        Student student = userService.authenticateStudent(email, password);
        if (student != null) {
            request.getSession().setAttribute("student", student);
            response.sendRedirect("");
        } else {
            request.setAttribute("message", "Invalid email or password.");
            doGet(request,response);
        }
    }
	
}

