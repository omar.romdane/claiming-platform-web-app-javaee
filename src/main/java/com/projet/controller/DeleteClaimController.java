package com.projet.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.projet.dao.ClaimDAOHibernateImp;
import com.projet.dao.ClaimDOA;
import com.projet.entity.Claim;
import com.projet.entity.Student;

@WebServlet(name="deleteClaim",urlPatterns = "/delete/*")
public class DeleteClaimController extends HttpServlet{
    /**
	 * 
	 */
	private static final long serialVersionUID = -703957718720061323L;
	private ClaimDOA claimDao = new ClaimDAOHibernateImp();
    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Student student = (Student) request.getSession().getAttribute("student");
        if(student == null) {
            response.sendRedirect("login");
            return;
        }
        int claimId = Integer.parseInt(request.getParameter("id"));
        Claim claim = claimDao.findClaimById(claimId);
        if(claim.getStudent().getId() != student.getId()) {
            response.sendRedirect("myclaims");
            return;
        }
        claimDao.deleteClaim(claim);
        response.sendRedirect("myclaims");
    }
}
