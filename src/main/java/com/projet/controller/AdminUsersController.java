package com.projet.controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.projet.dao.StudentDAOHibernateImp;
import com.projet.dao.StudentDoa;
import com.projet.entity.Student;

@WebServlet(name="adminUser",urlPatterns = "/admin/users/*")
public class AdminUsersController extends HttpServlet{
    /**
	 * 
	 */
	private static final long serialVersionUID = 7483985850716971180L;
	private StudentDoa studentDao = new StudentDAOHibernateImp();
    
	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if (action == null) {
            listUsers(request, response);
        } else if (action.equals("activate")) {
            activateUser(request, response);
        } else if (action.equals("deactivate")) {
            deactivateUser(request, response);
        } else if (action.equals("view")) {
            viewUser(request, response);
        } else {
            response.sendRedirect("users");
        }
    }
	private void listUsers(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Student> students = studentDao.findAll();
        request.setAttribute("students", students);
        request.getRequestDispatcher("/WEB-INF/AdminUsers.jsp").forward(request, response);
    }
	private void activateUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    String studentId = request.getParameter("id");
	    if (studentId == null) {
	        response.sendRedirect("users");
	        return;
	    }
	    Student student = studentDao.findById(Integer.parseInt(studentId));
	    if (student == null) {
	        response.sendRedirect("admin");
	        return;
	    }
	    student.setActive(true);
	    studentDao.update(student);
	    response.sendRedirect("users");
	}

	private void deactivateUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    String studentId = request.getParameter("id");
	    if (studentId == null) {
	        response.sendRedirect("users");
	        return;
	    }
	    Student student = studentDao.findById(Integer.parseInt(studentId));
	    if (student == null) {
	        response.sendRedirect("admin");
	        return;
	    }
	    student.setActive(false);
	    studentDao.update(student);
	    response.sendRedirect("users");
	}
	private void viewUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    String studentId = request.getParameter("id");
	    if (studentId == null) {
	        response.sendRedirect("users");
	        return;
	    }
	    Student student = studentDao.findById(Integer.parseInt(studentId));
	    if (student == null) {
	        response.sendRedirect("users");
	        return;
	    }
	    request.setAttribute("student", student);
	    request.getRequestDispatcher("/WEB-INF/AdminViewUser.jsp").forward(request, response);
	}

	
}
