package com.projet.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.projet.entity.Administrator;
import com.projet.service.UserService;

@WebServlet(name="adminLogin",urlPatterns = "/login/admin/*")
public class AdminLoginController extends HttpServlet{
	private static final long serialVersionUID = 495398352193329826L;
	private UserService userService = new UserService();
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/AdminLogin.jsp").include(request, response);   
	}
	@Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String email = request.getParameter("email");
	    String password = request.getParameter("password");
	    if(email.equals(null) || password.equals(null) || email.isEmpty() || password.isEmpty()){
	    	request.setAttribute("message", "Please fill out all fields.");
	    	doGet(request, response);
	    	return;
	    }
	    if (email.equals("admin@admin") && password.equals("admin")) {
	        Administrator administrator = new Administrator();
	        administrator.setEmail("admin@admin");
	        administrator.setPassword("admin");
	        request.getSession().setAttribute("administrator", administrator);
	        response.sendRedirect(request.getContextPath() + "/admin");
	    } else {
	        request.setAttribute("message", "Invalid email or password.");
	        doGet(request,response);
	    }
    }
}
