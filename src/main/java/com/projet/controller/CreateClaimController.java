package com.projet.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.projet.dao.ClaimDAOHibernateImp;
import com.projet.dao.ClaimDOA;
import com.projet.dao.StudentDAOHibernateImp;
import com.projet.dao.StudentDoa;
import com.projet.entity.Claim;
import com.projet.entity.ClaimType;
import com.projet.entity.Student;

@WebServlet(name="newClaim",urlPatterns = "/create-claim/*")
public class CreateClaimController extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private ClaimDOA claimDao = new ClaimDAOHibernateImp();
    private StudentDoa studentDao = new StudentDAOHibernateImp(); 

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        Student student = (Student) request.getSession().getAttribute("student");
        if(student == null) {
        	response.sendRedirect("login");
        }
        student = studentDao.findById(student.getId());
        if(!student.isActive()) {
        	request.setAttribute("message", "You can't make claims at the moment because your account is inactive. Contact the Page Admin in order to reactivate your account !");
        }
        List<ClaimType> claimTypes = claimDao.listClaimTypes();
        request.setAttribute("claimTypes", claimTypes);
        request.getRequestDispatcher("/WEB-INF/create-claim.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        Student student = (Student) request.getSession().getAttribute("student");
        if(student == null) {
        	response.sendRedirect("login");
        }
        student = studentDao.findById(student.getId());
        if(!student.isActive()) {
        	request.setAttribute("message", "You can't make claims at the moment because your account is inactive. Contact the Page Admin in order to reactivate your account !");
        	doGet(request,response);
        	return;
        }
        String title = request.getParameter("title");
        String description = request.getParameter("description");
        String type = request.getParameter("type");        
        if (title == null || description == null || type == null || title.isEmpty() || description.isEmpty() || type.isEmpty()) {
            request.setAttribute("message", "Please fill out all fields.");
            doGet(request, response);
            return;
        }
        ClaimType claimType = claimDao.findClaimTypeById(Integer.parseInt(type));
        if (claimType == null) {
            request.setAttribute("message", "Invalid claim type selected.");
            doGet(request, response);
            return;
        }
        Claim claim = new Claim(title, description, claimType, student);
        claimDao.addClaim(claim);
        response.sendRedirect("myclaims");
    }
}
