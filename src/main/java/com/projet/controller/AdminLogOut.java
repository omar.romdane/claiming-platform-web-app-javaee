package com.projet.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name="logout",urlPatterns = "/logout/admin/*")
public class AdminLogOut extends HttpServlet{
	private static final long serialVersionUID = 2337083527018193006L;
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    request.getSession().removeAttribute("administrator");
	    request.getSession().invalidate();
	    response.sendRedirect(request.getContextPath() + "/admin");
	}
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
