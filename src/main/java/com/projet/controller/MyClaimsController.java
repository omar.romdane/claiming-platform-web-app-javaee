package com.projet.controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.projet.dao.ClaimDAOHibernateImp;
import com.projet.dao.ClaimDOA;
import com.projet.entity.Claim;
import com.projet.entity.Student;

@WebServlet(name="claim-list",urlPatterns = "/myclaims/*")
public class MyClaimsController extends HttpServlet {
	private static final long serialVersionUID = -7969918519714954605L;
	private ClaimDOA claimDao = new ClaimDAOHibernateImp();
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Student loggedStudent = (Student) request.getSession().getAttribute("student");
        if(loggedStudent == null) {
        	response.sendRedirect("login");
        }
        List<Claim> claims = claimDao.listClaimsByStudent(loggedStudent);
        request.setAttribute("claims", claims);
        request.getRequestDispatcher("/WEB-INF/MyClaims.jsp").forward(request, response);
    }
}