package com.projet.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.projet.dao.ClaimDAOHibernateImp;
import com.projet.dao.ClaimDOA;
import com.projet.entity.ClaimType;

@WebServlet(name="adminClaimTypes",urlPatterns = "/admin/claimtypes/*")
public class AdminClaimTypeController extends HttpServlet{
    private ClaimDOA claimDao = new ClaimDAOHibernateImp();

	private static final long serialVersionUID = 4822500306020565361L;

	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if (action == null) {
            listClaimTypes(request, response);
        } else if (action.equals("create")) {
            showCreateClaimTypeForm(request, response);
        } else if (action.equals("edit")) {
            showEditClaimTypeForm(request, response);
        } else if (action.equals("delete")) {
            deleteClaimType(request, response);
        } else {
            response.sendRedirect("claimtypes");
        }
    }
	private void listClaimTypes(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<ClaimType> claimTypes = claimDao.listClaimTypes();
        request.setAttribute("claimTypes", claimTypes);
        request.getRequestDispatcher("/WEB-INF/AdminClaimTypes.jsp").forward(request, response);
    }
	private void deleteClaimType(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		ClaimType claimType = claimDao.findClaimTypeById(id);
		try {
		claimDao.deleteClaimType(claimType);
		}catch(Exception e){
			List<ClaimType> claimTypes = claimDao.listClaimTypes();
	        request.setAttribute("claimTypes", claimTypes);
			request.setAttribute("message", "Can't Delete ClaimType because there is claims wich claimtype is what you attended to delete");
			request.getRequestDispatcher("/WEB-INF/AdminClaimTypes.jsp").forward(request, response);
		}
		response.sendRedirect("claimtypes");
	}
	private void showCreateClaimTypeForm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/AdminCreateClaimType.jsp").forward(request, response);
    }
	private void showEditClaimTypeForm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        ClaimType claimType = claimDao.findClaimTypeById(id);
        request.setAttribute("claimType", claimType);
        request.getRequestDispatcher("/WEB-INF/AdminEditClaimType.jsp").forward(request, response);
    }

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    
	    String action = request.getParameter("action");
	    if(action == null) {
	    	response.sendRedirect("claimtypes");
	    }
        if (action.equals("create")) {
        	String name = request.getParameter("name");
        	if (name == null) {
    	        request.setAttribute("message", "Please fill out all fields.");
    	        showCreateClaimTypeForm(request, response);
    	        return;
    	    }
    	    ClaimType claimType = new ClaimType(name);
    	    claimDao.addClaimType(claimType);
        } else if (action.equals("update")) {
        	int id = Integer.parseInt(request.getParameter("id"));
            String name = request.getParameter("name");
            ClaimType claimType = claimDao.findClaimTypeById(id);
            claimType.setType(name);
            claimDao.updateClaimType(claimType);
            response.sendRedirect("claimtypes");
        }
	    
	    response.sendRedirect("claimtypes");
	}

}
