package com.projet.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.projet.dao.StudentDAOHibernateImp;
import com.projet.dao.StudentDoa;
import com.projet.entity.Student;
import com.projet.service.UserService;

@WebServlet(name="signup",urlPatterns = "/signup/*")
public class SignUpController extends HttpServlet{

	private static final long serialVersionUID = 402574224090079356L;
    private UserService userService = new UserService();
    private StudentDoa studentDoa = new StudentDAOHibernateImp();
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/SignUp.jsp").include(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        if(firstName.equals(null) || lastName.equals(null) || email.equals(null) || password.equals(null) || firstName.isEmpty() || lastName.isEmpty() || email.isEmpty() || password.isEmpty()){
        	//required fields
        	request.setAttribute("message", "Please fill out all fields.");
        	doGet(request, response);
        	return;
        }
        if (studentDoa.findByEmail(email) != null) {
            request.setAttribute("message", "Email already in use.");
            doGet(request, response);
            return;
        }
        if(userService.isValidPassword(password)) {
        	Student newStudent = new Student(email,password,true,firstName,lastName);
        	userService.createStudent(newStudent);
        	response.sendRedirect("login");
        }
    }
}


