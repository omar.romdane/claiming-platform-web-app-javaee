<%@page import="com.projet.entity.ClaimType"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<title>Edit Claim Type</title>
<%@ include file="components/head.jsp" %>
</head>
<body>
    <% ClaimType claimType = (ClaimType) request.getAttribute("claimType"); %>
  <%@ include file="components/AdminNavbar.jsp" %>
	<section class="banner-area relative about-banner" id="home">	
		<div class="overlay overlay-bg"></div>
		<div class="container">				
			<div class="row d-flex align-items-center justify-content-center">
				<div class="about-content col-lg-12">
					<h1 class="text-white">
						Edit Claim Type				
					</h1>	
					<p class="text-white link-nav"><a href="<%= adminUrl %>">Admin</a>  <span class="lnr lnr-arrow-right"></span>  <a href="">Edit Claim Type</a></p>
				</div>	
			</div>
		</div>
	</section>
    <center><div class="container">
        <h2>Edit Claim Type</h2>
        <form action="claimtypes?action=update" method="post">
            <input type="hidden" name="action" value="update"><br>
            <input type="hidden" name="id" value="<%= claimType.getId() %>"><br>
            <div>
            <label>Name:</label><br>
            <input type="text" name="name" value="<%= claimType.getType() %>" required>
        </div>
        <div>
            <input type="submit" value="Update"  class="genric-btn primary" style="margin: 25px;" >
        </div>
    </form>
</div></center>
    <%@ include file="components/scripts.jsp" %>
</body>
</html>
