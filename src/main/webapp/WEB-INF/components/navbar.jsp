<%@page import="com.projet.entity.Student"%>
<header id="header" id="home">
	<div class="header-top">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-sm-6 col-8 header-top-left no-padding">			
				</div>
				<div class="col-lg-6 col-sm-6 col-4 header-top-right no-padding">
					<a href="tel:+216 93 880 141"><span class="lnr lnr-phone-handset"></span> <span class="text">+(216) 93 880 141</span></a>
					<a href="mailto:omar.romdhane@etudiant-enit.utm.tn"><span class="lnr lnr-envelope"></span> <span class="text">omar.romdhane@etudiant-enit.utm.tn</span></a>			
				</div>
			</div>			  					
		</div>
  </div>
	<div class="container main-menu">
		<div class="row align-items-center justify-content-between d-flex">
			<div id="logo">
				<a href="<%= contextPath %>"><img src="<%= contextPath %>public/img/logo.png" alt="" title="" /></a>
			</div>
			<nav id="nav-menu-container">
				<ul class="nav-menu">
					<li><a href="<%= contextPath %>">Home</a></li>
					<li class="menu-has-children"><a href="">Claim</a>
						<ul>
							<li><a href="<%= contextPath %>/myclaims">My Claims</a></li>
							<li><a href="<%= contextPath %>/create-claim">New Claim</a></li>
						</ul>
					</li>
					<li class="menu-has-children"><a href="">Account</a>
						<ul>
							<% 
							  Student user = (Student)request.getSession().getAttribute("student");
							  if(user == null) {
						  		%>
								  <li><a href="<%= contextPath %>/login">Login</a></li>
								  <li><a href="<%= contextPath %>/signup">Sign Up</a></li>
								  <%
								}
							  if(user != null) {
									%>
									<li><a href="logout">Sign out <%user.getEmail(); %></a></li>
							<% }%>
						</ul>
					</li>
				</ul>
			</nav>
		</div>
	</div>
</header>