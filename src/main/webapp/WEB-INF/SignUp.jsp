<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>

<head>

	<title>Sign Up</title>
	<%@ include file="components/head.jsp" %>

</head>

<body>
	<%@ include file="components/navbar.jsp" %>
	<section class="banner-area relative about-banner" id="home">
		<div class="overlay overlay-bg"></div>
		<div class="container">
			<div class="row d-flex align-items-center justify-content-center">
				<div class="about-content col-lg-12">
					<h1 class="text-white">
						Sign up
					</h1>
					<p class="text-white link-nav"><a href="<%= contextPath %>">Home </a> <span
							class="lnr lnr-arrow-right"></span> <a href="<%= contextPath %>/signup">Sign Up</a></p>
				</div>
			</div>
		</div>
	</section>
	<center>
		<div class="container">
			<% if (request.getAttribute("message") != null) { %>
			<div class="alert alert-warning">
				<p class="error-message"><%= request.getAttribute("message") %></p>
			</div>
			<% } %>
			<div class="" style="margin-top: 50px;">

				<form method="post" action="signup">
					<label for="firstName">First Name</label><br>
					<input type="text" id="firstName" name="firstName" required><br>
					<label for="lastName">Last Name</label><br>
					<input type="text" id="lastName" name="lastName" required><br>
					<label for="email">Email:</label><br>
					<input type="email" id="email" name="email" required><br>
					<label for="password">Password:</label><br>
					<input type="password" value="" name="password" id="password" required><br>
					<input type="submit" value="Sign Up" id="Submit" class="genric-btn primary" style="margin: 25px;">
				</form>
				<a href="<%= contextPath %>/login">Already have and account ? Sign In here.</a>
			</div>
		</div>
		<%@ include file="components/scripts.jsp" %>

	</center>
</body>

</html>