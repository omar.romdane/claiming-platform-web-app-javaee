<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en" class="no-js">

<head>

	<title>home page</title>
	<%@ include file="components/head.jsp" %>

</head>

<body>
	<%@ include file="components/navbar.jsp" %>

	<section class="banner-area relative" id="home">
		<div class="overlay overlay-bg"></div>
		<div class="container">
			<div class="row fullscreen d-flex align-items-center justify-content-between">
				<div class="banner-content col-lg-9 col-md-12">
					<h1 class="text-uppercase">
						We Ensure better education
						for a better world
					</h1>
					<p class="pt-10 pb-10">
						In the history of modern astronomy, there is probably no one greater leap forward than the
						building and launch of the space telescope known as the Hubble.
					</p>
					<a href="<%= contextPath %>/create-claim" class="primary-btn text-uppercase">Get Started</a>
				</div>
			</div>
		</div>
	</section>

	<%@ include file="components/scripts.jsp" %>

</body>

</html>