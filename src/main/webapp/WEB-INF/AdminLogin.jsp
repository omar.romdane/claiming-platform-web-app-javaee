<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en" class="no-js">

<head>

    <title>Login</title>
    <%@ include file="components/head.jsp" %>

</head>

<body>
    <%@ include file="components/AdminNavbar.jsp" %>
    <section class="banner-area relative about-banner" id="home">
        <div class="overlay overlay-bg"></div>
        <div class="container">
            <div class="row d-flex align-items-center justify-content-center">
                <div class="about-content col-lg-12">
                    <h1 class="text-white">
                        Login
                    </h1>
                    <p class="text-white link-nav"><a href="<%= contextPath %>">Admin</a> <span
                            class="lnr lnr-arrow-right"></span> <a href="<%= contextPath %>/login/admin">Login</a></p>
                     
                </div>
            </div>
        </div>
    </section>
    <center>
        <div class="container">
            <% if (request.getAttribute("message") != null) { %>
            <div class="alert alert-warning" role="alert">
                <p class="error-message"><%= request.getAttribute("message") %></p>
            </div>
            <% } %>
            <div class="login container" style="margin-top: 50px;">
                <form method="post" action="<%= contextPath %>/login/admin">
                    <label for="email"><p>Email:</p></label><br>
                    <input type="email" id="email" name="email" required><br>
                    <label for="password"><p>Password:</p></label><br>
                    <input type="password" value="" name="password" id="password" required><br>
                    <input type="submit" value="Login" id="Submit" class="genric-btn info" style="margin: 25px;">
                </form>
                <p>hint : adress is "admin@admin" and passwor is "admin". security is our main concern ;).</p>
            </div>
        </div>
    </center>
    <%@ include file="components/scripts.jsp" %>

</body>

</html>