<%@page import="com.projet.entity.ClaimType"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>

<head>
	<title>Claim Types</title>
	<%@ include file="components/head.jsp" %>
</head>

<body>
	<%@ include file="components/AdminNavbar.jsp" %>
	<section class="banner-area relative about-banner" id="home">
		<div class="overlay overlay-bg"></div>
		<div class="container">
			<div class="row d-flex align-items-center justify-content-center">
				<div class="about-content col-lg-12">
					<h1 class="text-white">
						Claim Types
					</h1>
					<p class="text-white link-nav"><a href="<%= adminUrl %>">Admin</a> <span
							class="lnr lnr-arrow-right"></span> <a href="">Claim Types</a></p>
				</div>
			</div>
		</div>
	</section>
	<center>
		<div class="container">
			<% if (request.getAttribute("message") != null) { %>
			<div class="alert alert-warning" role="alert">
				<p class="error-message"><%= request.getAttribute("message") %></p>
			</div>
			<% } %>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th scope="col">Type</th>
						<th scope="col">Actions</th>
					</tr>
				</thead>
				<tbody>
					<% 
						List<ClaimType> claimTypes = (List<ClaimType>)request.getAttribute("claimTypes");
							for( ClaimType type : claimTypes) { 
								%>
					<tr>
						<td><%= type.getType() %></td>
						<td>
							<a href="<%= adminUrl %>/claimtypes?action=edit&id=<%= type.getId() %>" class="genric-btn info">Edit</a> |
							<a href="<%= adminUrl %>/claimtypes?action=delete&id=<%= type.getId() %>"
								onclick="return confirm('Are you sure you want to delete this claim?');"
								class="genric-btn danger">Delete</a>

						</td>
					</tr>
					<% } %>
				</tbody>
			</table>

			<a href="<%= adminUrl %>/claimtypes?action=create" class="genric-btn info" style="width: 100%;margin-bottom: 50px;">Add New
				Claim Type</a>
		</div>
	</center>
	<%@ include file="components/scripts.jsp" %>

</body>

</html>
