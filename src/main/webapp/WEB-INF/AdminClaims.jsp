<%@page import="java.util.List"%>
<%@page import="com.projet.entity.ClaimType"%>
<%@page import="com.projet.entity.Claim"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
  <title>List Claims</title>
  <%@ include file="components/head.jsp" %>
</head>

<body>
  <%@ include file="components/AdminNavbar.jsp" %>
  <section class="banner-area relative about-banner" id="home">
    <div class="overlay overlay-bg"></div>
    <div class="container">
      <div class="row d-flex align-items-center justify-content-center">
        <div class="about-content col-lg-12">
          <h1 class="text-white">
            Claims
          </h1>
          <p class="text-white link-nav"><a href="<%= adminUrl %>">Admin</a> <span class="lnr lnr-arrow-right"></span>
            <a href="">Claims</a></p>
        </div>
      </div>
    </div>
  </section>

    <div class="container">

      <h1>List Claims</h1>
      <form action="claims" method="get">
        <label for="claimTypeId">Claim Type: </label>
        <select name="claimTypeId" id="claimTypeId">
          <option value="">All</option>
          <% 
          List<ClaimType> claimTypes = (List<ClaimType>)request.getAttribute("claimTypes");
            ClaimType selectedClaimType = (ClaimType)request.getAttribute("selectedClaimType");
            String search = (String) request.getAttribute("search");
            if(search == null){search = "";}
            for (ClaimType claimType : claimTypes) { %>
          <option value="<%=claimType.getId()%>"
            <% if (selectedClaimType != null && selectedClaimType.getId() == claimType.getId()) { %> selected <% } %>>
            <%=claimType.getType()%>
          </option>
          <% } %>
        </select>
        <br>
        <label for="search"> Search: </label><br>   
        <input type="text" name="search" id="search" value="<%=search%>" >
        <br>
        <input type="submit" value="Filter" style="margin: 25px 0;" class="genric-btn info">
      </form>
      <table class="table table-bordered">
        <tr>
          <th>Title</th>
          <th>Description</th>
          <th>Claim Type</th>
          <th>Actions</th>
        </tr>
        <% 
        List<Claim> claims = (List<Claim>)request.getAttribute("claims");
          for (Claim claim : claims) { %>
        <tr>
          <td><%=claim.getTitle()%></td>
          <td><%=claim.getDescription()%></td>
          <td><%=claim.getClaimType().getType()%></td>
          <td>
            <a href="<%= adminUrl %>/claims?action=view&id=<%=claim.getId()%>" class="genric-btn info" style="width: 100%;">View</a>

            <a href="<%= adminUrl %>/claims?action=delete&id=<%=claim.getId()%>"
              class="genric-btn danger" onclick="return confirm('Are you sure you want to delete this claim?');" style="width: 100%;">delete</a>
          </td>

        </tr>
        <% } %>
      </table>
    </div>
    
  <%@ include file="components/scripts.jsp" %>

</body>

</html>