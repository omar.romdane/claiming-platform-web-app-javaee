<%@page import="com.projet.entity.Student"%>
<%@page import="com.projet.entity.Claim"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<title>View Student</title>
<%@ include file="components/head.jsp" %>

</head>
<body>
	<%
	Student student = (Student) request.getAttribute("student");
	%>
  <%@ include file="components/AdminNavbar.jsp" %>
	<section class="banner-area relative about-banner" id="home">	
		<div class="overlay overlay-bg"></div>
		<div class="container">				
			<div class="row d-flex align-items-center justify-content-center">
				<div class="about-content col-lg-12">
					<h1 class="text-white">
						View Student				
					</h1>	
					<p class="text-white link-nav"><a href="<%= adminUrl %>">Admin</a>  <span class="lnr lnr-arrow-right"></span>  <a href="">View Student</a></p>
				</div>	
			</div>
		</div>
	</section>
<center><div class="container">
	<table class="table table-bordered">
		<tr>
			<th>firstName</th>
			<th>lastName</th>
			<th>Email</th>
			<th>Active</th>
			<th>Actions</th>
		</tr>
		<tr>
			
			<td><%= student.getFirstName() %></td>
			<td><%= student.getLastName() %></td>
			<td><%= student.getEmail() %></td>
			<td><%= student.isActive() ? "Yes" : "No" %></td>
			<td>
				<a href="<%= adminUrl %>/users?action=activate&id=<%= student.getId() %>" class="genric-btn success" style="width: 100%;">Activate</a>
				<a href="<%= adminUrl %>/users?action=deactivate&id=<%= student.getId() %>" class="genric-btn danger" style="width: 100%;">Deactivate</a>
			</td>
		</tr>
	</table>
	</div>
	<div class="container">
	<p>Student claims</p>
	<table class="table table-bordered">
		<tr>
			<th>Title</th>
			<th>Description</th>
			<th>Type</th>
			<th>Actions</th>
		</tr>
		<%
		List<Claim> claims = student.getClaims();
			for (Claim claim : claims) {
				%>
				<tr>
					<td><%= claim.getTitle() %></td>
					<td><%= claim.getDescription() %></td>
					<td><%= claim.getClaimType().getType() %></td>
					<td>
						<a href="<%= adminUrl %>/claims?action=delete&id=<%= claim.getId() %>">Delete</a>
					</td>
				</tr>
				<%
			}
			%>
		</table>
	</div></center>
		<%@ include file="components/scripts.jsp" %>
		
	</body>
	</html>