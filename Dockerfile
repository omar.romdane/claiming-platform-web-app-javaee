# Use a Java runtime as the base image
FROM bitnami/wildfly:26

# Set the working directory in the container
WORKDIR /app

# Copy the Java EE application files to the container
COPY ./web-projet-2.war /app

# Expose the port on which the Java EE application runs (adjust the port number if necessary)
EXPOSE 8080 9990

