<%@page import="com.projet.entity.Student"%>
<%@page import="com.projet.entity.User"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>

<head>
    <%@ include file="components/head.jsp" %>
    <title>Users</title>
</head>

<body>
    <%@ include file="components/AdminNavbar.jsp" %>
    <section class="banner-area relative about-banner" id="home">
        <div class="overlay overlay-bg"></div>
        <div class="container">
            <div class="row d-flex align-items-center justify-content-center">
                <div class="about-content col-lg-12">
                    <h1 class="text-white">
                        Students
                    </h1>
                    <p class="text-white link-nav"><a href="<%= adminUrl %>/admin">Admin</a> <span
                            class="lnr lnr-arrow-right"></span> <a href="">Students</a></p>
                </div>
            </div>
        </div>
    </section>
    <center>
        <div class="container">
            <table class="table table-bordered">
                <tr>
                    <th scope="col">firstName</th>
                    <th scope="col">lastName</th>
                    <th scope="col">Email</th>
                    <th scope="col">Active</th>
                    <th scope="col">Actions</th>
                </tr>
                <% 
    for(Student student : (List<Student>)request.getAttribute("students")) { 
        %>
                <tr>
                    <td><%= student.getFirstName() %></td>
                    <td><%= student.getLastName() %></td>
                    <td><%= student.getEmail() %></td>
                    <td><%= student.isActive() ? "Yes" : "No" %></td>
                    <td>
                        <a href="<%= adminUrl %>/users?action=activate&id=<%= student.getId() %>" class="genric-btn success" style="width: 100%;"
                            style="margin: 25px;">Activate</a>
                        <a href="<%= adminUrl %>/users?action=deactivate&id=<%= student.getId() %>" class="genric-btn danger" style="width: 100%;">Deactivate</a>
                        <a href="<%= adminUrl %>/users?action=view&id=<%= student.getId() %>" class="genric-btn info" style="width: 100%;">View</a>
                    </td>
                </tr>
                <% } %>
                <%@ include file="components/scripts.jsp" %>

            </table>
        </div>
    </center>

</body>

</html>