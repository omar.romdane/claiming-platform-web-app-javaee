<%@page import="com.projet.entity.Administrator"%>
<header id="header" id="home">
	<div class="header-top">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-sm-6 col-8 header-top-left no-padding">			
				</div>
				<div class="col-lg-6 col-sm-6 col-4 header-top-right no-padding">
					<a href="tel:+216 93 880 141"><span class="lnr lnr-phone-handset"></span> <span class="text">+(216) 93 880 141</span></a>
					<a href="mailto:omar.romdhane@etudiant-enit.utm.tn"><span class="lnr lnr-envelope"></span> <span class="text">omar.romdhane@etudiant-enit.utm.tn</span></a>			
				</div>
			</div>			  					
		</div>
  </div>
	<div class="container main-menu">
		<div class="row align-items-center justify-content-between d-flex">
			<div id="logo">
				<a href="<%= adminUrl %>"><img src="<%= contextPath %>public/img/logo.png" alt="" title="" /></a>
			</div>
			<nav id="nav-menu-container">
				<ul class="nav-menu">
					<li><a href="<%= adminUrl %>">Home</a></li>
					<li><a href="<%= adminUrl %>/claims">Claims</a></li>
					<li><a href="<%= adminUrl %>/claimtypes">ClaimTypes</a></li>
					<li><a href="<%= adminUrl %>/users">Users</a></li>
					<% 
						Administrator user = (Administrator)request.getSession().getAttribute("administrator");
						if(user != null) {
						%>
							<li class="menu-has-children"><a href="">Account</a>
								<ul>
									<li><a href="<%= contextPath %>/logout/admin">Sign out</a></li>
								</ul>
							</li>
						<%
						}%>
					
				</ul>
			</nav>
		</div>
	</div>
</header>