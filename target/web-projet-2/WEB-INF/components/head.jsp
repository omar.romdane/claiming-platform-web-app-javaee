		<%
    		String contextPath = request.getContextPath();
			String adminUrl = contextPath + "/admin";
		%>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="shortcut icon" href="public/img/fav.png">
		<meta name="author" content="omar romdhane">
		<meta name="description" content="">
		<meta name="keywords" content="">
		<meta charset="UTF-8">
		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">

		<link rel="stylesheet" href="<%= contextPath %>/public/css/linearicons.css">
		<link rel="stylesheet" href="<%= contextPath %>/public/css/font-awesome.min.css">
		<link rel="stylesheet" href="<%= contextPath %>/public/css/bootstrap.css">
		<link rel="stylesheet" href="<%= contextPath %>/public/css/magnific-popup.css">
		<link rel="stylesheet" href="<%= contextPath %>/public/css/nice-select.css">
		<link rel="stylesheet" href="<%= contextPath %>/public/css/animate.min.css">
		<link rel="stylesheet" href="<%= contextPath %>/public/css/owl.carousel.css">
		<link rel="stylesheet" href="<%= contextPath %>/public/css/jquery-ui.css">
		<link rel="stylesheet" href="<%= contextPath %>/public/css/main.css">
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
			type="text/css" />
		<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
		<link href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.3.2/css/simple-line-icons.css" rel="stylesheet"
			type="text/css">