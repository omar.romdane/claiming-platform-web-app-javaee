<%@page import="com.projet.entity.ClaimType"%>
<%@ page import="java.util.List" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
  <title>Create Claim</title>
  <%@ include file="components/head.jsp" %>
</head>

<body>
  <%@ include file="components/navbar.jsp" %>
  <section class="banner-area relative about-banner" id="home">
    <div class="overlay overlay-bg"></div>
    <div class="container">
      <div class="row d-flex align-items-center justify-content-center">
        <div class="about-content col-lg-12">
          <h1 class="text-white">
            New Claim
          </h1>
          <p class="text-white link-nav"><a href="<%= contextPath %>">Home </a> <span class="lnr lnr-arrow-right"></span> <a
              href="<%= contextPath %>/create-claim">New Claim</a></p>
        </div>
      </div>
    </div>
  </section>
  <center>
    <div class="container">
      <% if (request.getAttribute("message") != null) { %>
      <div class="alert alert-warning" role="alert">
        <p class="error-message"><%= request.getAttribute("message") %></p>
      </div>
      <% } %>
      <h1>Create a new Claim</h1>
      <form action="create-claim" method="post">
        <label for="title">Title:</label><br>
        <input type="text" id="title" name="title">
        <br>
        <label for="description">Description:</label><br>
        <textarea id="description" name="description"></textarea>
        <br>
        <label for="claimType">Claim Type:</label><br>
        <select id="claimType" name="type"><br>
          <% 
          List<ClaimType> claimTypes = (List<ClaimType>) request.getAttribute("claimTypes");
            if(claimTypes != null){
              for( ClaimType type : claimTypes) { %>
          <option value="<%= type.getId() %>" for="type"><%= type.getType() %></option>
          <%}
              }%>

        </select>
        <br>
        <input type="submit" value="Create Claim" class="genric-btn primary" style="margin: 25px;">
      </form>
    </div>
  </center>
  <%@ include file="components/scripts.jsp" %>

</body>

</html>
