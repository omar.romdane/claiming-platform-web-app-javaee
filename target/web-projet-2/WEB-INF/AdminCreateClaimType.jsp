<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<title>Create Claim Type</title>
<%@ include file="components/head.jsp" %>
</head>
<body>
  <%@ include file="components/AdminNavbar.jsp" %>
	<section class="banner-area relative about-banner" id="home">	
		<div class="overlay overlay-bg"></div>
		<div class="container">				
			<div class="row d-flex align-items-center justify-content-center">
				<div class="about-content col-lg-12">
					<h1 class="text-white">
						Create Claim Type				
					</h1>	
					<p class="text-white link-nav"><a href="<%= adminUrl %>">Admin</a>  <span class="lnr lnr-arrow-right"></span>  <a href="">Create Claim Type</a></p>
				</div>	
			</div>
		</div>
	</section>
	<center><div class="container">
		<h1>Create Claim Type</h1>
		<form action="claimtypes?action=create" method="post">
			<label for="name">Name:</label><br>
			<input type="text" id="name" name="name"><br>
			<input type="submit" value="Create"  class="genric-btn primary" style="margin: 25px;">
		</form>
	</div></center>
    <%@ include file="components/scripts.jsp" %>

</body>
</html>
