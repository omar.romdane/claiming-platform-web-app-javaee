<%@page import="com.projet.entity.Claim"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
  <title>View Claim</title>
  <%@ include file="components/head.jsp" %>
</head>

<body>
  <%@ include file="components/AdminNavbar.jsp" %>
  <section class="banner-area relative about-banner" id="home">
    <div class="overlay overlay-bg"></div>
    <div class="container">
      <div class="row d-flex align-items-center justify-content-center">
        <div class="about-content col-lg-12">
          <h1 class="text-white">
            View Claim
          </h1>
          <p class="text-white link-nav"><a href="<%= adminUrl %>">Admin</a> <span class="lnr lnr-arrow-right"></span>
            <a href="">View Claim</a></p>
        </div>
      </div>
    </div>
  </section>
  <center>
    <div class="container">
      <h1>View Claim</h1>
      <% 
      Claim claim = (Claim)request.getAttribute("claim");
      %>
      <table class="table">
        <tr>
          <th scope="col">Title</th>
          <td scope="row"><%=claim.getTitle()%></td>
        </tr>
        <tr>
          <th scope="col">Description</th>
          <td scope="row"><%=claim.getDescription()%></td>
        </tr>
        <tr>
          <th scope="col">Claim Type</th>
          <td scope="row"><%=claim.getClaimType().getType()%></td>
        </tr>
        <tr>
          <th>Student Email</th>
          <td scope="row"><a href="<%= adminUrl %>/users?action=view&id=<%= claim.getStudent().getId() %>"><%=claim.getStudent().getEmail()%></a></td>
        </tr>
      </table>
    </div>
  </center>
  <%@ include file="components/scripts.jsp" %>

</body>

</html>