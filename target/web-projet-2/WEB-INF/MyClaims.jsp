<%@page import="com.projet.entity.Claim"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>

	<title>My Claims</title>
	<%@ include file="components/head.jsp" %>

</head>

<body>
	<%@ include file="components/navbar.jsp" %>

	<section class="banner-area relative about-banner" id="home">	
		<div class="overlay overlay-bg"></div>
		<div class="container">				
			<div class="row d-flex align-items-center justify-content-center">
				<div class="about-content col-lg-12">
					<h1 class="text-white">
						My Claims				
					</h1>	
					<p class="text-white link-nav"><a href="<%= contextPath %>">Home </a>  <span class="lnr lnr-arrow-right"></span>  <a href="<%= contextPath %>/login">My Claims</a></p>
				</div>	
			</div>
		</div>
	</section>
	<center>
		<div class="container">
			<table class="table">
				<tr>
					<th scope="col">Title</th>
					<th scope="col">Description</th>
					<th scope="col">Type</th>
					<th scope="col">Actions</th>
				</tr>
				<%
				List<Claim> claims = (List<Claim>) request.getAttribute("claims");
					for (Claim claim : claims) {
						%>
						<tr scope="row">
						<td><%= claim.getTitle() %></td>
						<td><%= claim.getDescription() %></td>
						<td><%= claim.getClaimType().getType() %></td>
						<td>
							<a href="<%= contextPath %>/delete?id=<%= claim.getId() %>" class="genric-btn danger circle" onclick="return confirm('Are you sure you want to delete this claim?');">Delete</a>
						</td>
					</tr>
					<%
				}
				%>
			</table>
		</div>
	</center>
	<%@ include file="components/scripts.jsp" %>

</body>

</html>
