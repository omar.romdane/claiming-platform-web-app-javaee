<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en" class="no-js">

<head>

    <title>Login</title>
    <%@ include file="components/head.jsp" %>

</head>

<body>
    <%@ include file="components/navbar.jsp" %>
    <section class="banner-area relative about-banner" id="home">
        <div class="overlay overlay-bg"></div>
        <div class="container">
            <div class="row d-flex align-items-center justify-content-center">
                <div class="about-content col-lg-12">
                    <h1 class="text-white">
                        Login
                    </h1>
                    <p class="text-white link-nav"><a href="<%= contextPath %>">Home </a> <span
                            class="lnr lnr-arrow-right"></span> <a href="<%= contextPath %>/login">Login</a></p>
                </div>
            </div>
        </div>
    </section>
    <center>
        <div class="container">
            <% if (request.getAttribute("message") != null) { %>
            <div class="alert alert-warning" role="alert">
                <p class="error-message"><%= request.getAttribute("message") %></p>
            </div>
            <% } %>
            <div class="login container" style="margin-top: 50px;">
                <form method="post" action="login">
                    <label for="email"><p>Email:</p></label><br>
                    <input type="email" id="email" name="email" required><br>
                    <label for="password"><p>Password:</p></label><br>
                    <input type="password" value="" name="password" id="password" required><br>
                    <input type="submit" value="Login" id="Submit" class="genric-btn primary" style="margin: 25px;">
                </form>
                <a href="<%= contextPath %>/signup">Don't have and account ? Sign Up here.</a>
            </div>
        </div>
    </center>
    <%@ include file="components/scripts.jsp" %>

</body>

</html>